package jp.co.uniquevision.injecttest;

import java.lang.annotation.Retention;
import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by tajima-junpei on 13/12/09.
 */
@Qualifier @Retention(RUNTIME)
public @interface ForApplication {
}
