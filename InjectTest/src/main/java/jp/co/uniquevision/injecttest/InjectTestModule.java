package jp.co.uniquevision.injecttest;

import dagger.Module;
import jp.co.uniquevision.injecttest.ui.MainActivity;

/**
 * Created by tajima-junpei on 13/12/09.
 */

@Module(
        injects = MainActivity.class,
        complete = false
)

public class InjectTestModule {
}
