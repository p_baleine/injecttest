package jp.co.uniquevision.injecttest;

import android.content.Context;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tajima-junpei on 13/12/09.
 *
 * ContextかApplicationを要求するAndroid向け依存のためのモジュール
 */
@Module(library = true)
public class AndroidModule {
    private final InjectTestApplication application;

    public AndroidModule(InjectTestApplication application) {
        this.application = application;
    }

    /**
     * アプリケーションコンテキストのインジェクトを可能にする
     * アクティビティコンテキストと区別するために明示的に
     * ForApplicationでアノテートする必要がある
     */
    @Provides @Singleton @ForApplication Context provideApplicationContext() {
        return application;
    }

    @Provides @Singleton LocationManager provideLocationManager() {
        return (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
    }
}
