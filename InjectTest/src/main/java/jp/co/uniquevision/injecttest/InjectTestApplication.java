package jp.co.uniquevision.injecttest;

import android.app.Application;
import dagger.ObjectGraph;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tajima-junpei on 13/12/09.
 */
public class InjectTestApplication extends Application {
    private ObjectGraph graph;

    @Override public void onCreate() {
        super.onCreate();

        graph = ObjectGraph.create(getModules().toArray());
    }

    protected List<Object> getModules() {
        return Arrays.asList(
                new AndroidModule(this),
                new InjectTestModule()
        );
    }

    public void inject(Object object) {
        graph.inject(object);
    }
}
