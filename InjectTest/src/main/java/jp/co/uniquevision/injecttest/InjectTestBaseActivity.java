package jp.co.uniquevision.injecttest;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by tajima-junpei on 13/12/09.
 */
public class InjectTestBaseActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((InjectTestApplication) getApplication()).inject(this);
    }
}
